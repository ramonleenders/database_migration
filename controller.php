<?php namespace Concrete\Package\DatabaseMigration;

defined('C5_EXECUTE') or die("Access Denied.");

use Config;
use Database;
use Package;
use Page;
use SinglePage;

class Controller extends Package
{
	protected $pkgHandle = 'database_migration';
	protected $appVersionRequired = '5.7.4.0';
	protected $pkgVersion = '1.0.0';

	public function getPackageName()
	{
		return t('Database Migration');
	}

	public function getPackageDescription()
	{
		return t('Migrate your database from lowercase to case sensitive tables or vice versa');
	}

	public function install()
	{
		$pkg = parent::install();
		SinglePage::add('/dashboard/system/database_migration', $pkg);
	}

	public function upgrade()
	{
		// Old page, we need to delete it
		$page = Page::getByPath('/dashboard/system/backup/database_migration');
		if (is_object($page) && !$page->isError()) {
			$page->delete();
		}
		// New page (without /backup), add it if it's not exists yet
		$pageDbMigrationPath = '/dashboard/system/database_migration';
		$pageDbMigration = Page::getByPath($pageDbMigrationPath);
		if ($pageDbMigration->isError() || (!is_object($pageDbMigration))) {
			SinglePage::add($pageDbMigrationPath, $this);
		}

		parent::upgrade();
	}

	public function getDatabaseMigrationTables()
	{
		$concreteVersion = Config::get('concrete.version');
		$tables = array(
            'CalendarEventSearchIndexAttributes',
            'CollectionSearchIndexAttributes',
			'FileSearchIndexAttributes',
			'OauthUserMap',
			'SystemDatabaseMigrations',
			'UserSearchIndexAttributes',
		);
		if (version_compare($concreteVersion, '8.0.0', '>=')) {
			$db = Database::connection();
			$expressEntities = $db->fetchAll('SELECT * FROM ExpressEntities');
			foreach ($expressEntities as $expressEntity) {
				$expressEntityHandle = $expressEntity['handle'];
				$expressEntityHandleNameSpace = implode('', array_map(function ($v, $k) {
					return ucFirst($v);
				}, explode('_', $expressEntityHandle), array_keys(explode('_', $expressEntityHandle))));
				$tables[] = $expressEntityHandleNameSpace . 'ExpressSearchIndexAttributes';
			}
			$tables[] = 'SiteSearchIndexAttributes';
		}

		return $tables;
	}
}